#!/bin/bash

echo 'Initial deployment of the service watches-v2'
kubectl create -f watches-v2-deployment.yaml

echo 'Exposing the watches-v1 deployment as a service internally.'
kubectl expose deployment watches-v2 --target-port=80 --type=NodePort

