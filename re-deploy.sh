#!/bin/bash

current_version=`cat version.txt`
echo "Deploying the watches-v2 service with version: $current_version"
kubectl set image deployment watches-v2 watches-v2=harperkej/watches-v2:"$current_version"
