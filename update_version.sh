#!/bin/bash

current_version=`cat version.txt`

echo "Current version = $current_version"

next_version=$(($current_version+1))
echo "$next_version" > version.txt

echo "Next version: $next_version"
