package cloudcomputing.assignment2.type;

public enum WatchStatus {
    old,
    current,
    outlet
}
