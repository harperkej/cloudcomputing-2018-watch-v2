package cloudcomputing.assignment2.repository;

import cloudcomputing.assignment2.entity.WatchEntity;
import cloudcomputing.assignment2.exception.ApplicationException;
import cloudcomputing.assignment2.exception.ExceptionReason;
import cloudcomputing.assignment2.type.Gender;
import cloudcomputing.assignment2.type.WatchStatus;
import cloudcomputing.assignment2.type.WatchType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class WatchRepository {

    @Autowired
    private EntityManager entityManager;

    public WatchEntity create(WatchEntity watchEntity) throws ApplicationException {
        String sku = watchEntity.getSku();
        this.validateWatchBeforePersist(sku);
        this.entityManager.persist(watchEntity);
        this.entityManager.flush();
        return watchEntity;
    }

    public WatchEntity findBySky(String sku) {
        final String queryStr = "SELECT w FROM WatchEntity w WHERE w.sku = :sku";
        Query query = this.entityManager.createQuery(queryStr);
        query.setParameter("sku", sku);
        WatchEntity result = null;
        try {
            result = (WatchEntity) query.getSingleResult();
        } catch (NoResultException noResultExcpetion) {
        }
        return result;
    }

    public WatchEntity update(WatchEntity watchEntity) {
        return this.entityManager.merge(watchEntity);
    }

    public void delete(WatchEntity watchEntity) {
        this.entityManager.remove(watchEntity);
    }

    public List<WatchEntity> search(String sku, WatchType type, WatchStatus status, Gender gender, Integer year) {
        StringBuilder queryStr = new StringBuilder();
        queryStr.append("SELECT w FROM WatchEntity w WHERE ");
        boolean searchBySku = sku != null;
        boolean searchByType = type != null;
        boolean searchByStatus = status != null;
        boolean searchByGender = gender != null;
        boolean searchByYear = year != null;

        if (searchBySku) {
            queryStr.append("w.sku like :sku ");
        }

        if (searchByType) {
            if (searchBySku) {
                queryStr.append("OR ");
            }
            queryStr.append("w.type = :type ");
        }

        if (searchByStatus) {
            queryStr.append("OR w.status = :status ");
        }
        if (searchByGender) {
            queryStr.append("OR w.gender = :gender ");
        }
        if (searchByYear) {
            queryStr.append("OR w.year = :year");
        }

        if (!searchBySku && !searchByType && !searchByStatus && !searchByGender && !searchByYear) {
            queryStr.append("1=1 ");
        }

        Query query = this.entityManager.createQuery(queryStr.toString());
        if (searchBySku) {
            query.setParameter("sku", "%" + sku + "%");
        }

        if (searchByType) {
            query.setParameter("type", type);
        }

        if (searchByStatus) {
            query.setParameter("status", status);
        }
        if (searchByGender) {
            query.setParameter("gender", gender);
        }
        if (searchByYear) {
            query.setParameter("year", year);
        }

        return query.getResultList();
    }

    public List<WatchEntity> findAll() {
        final String queryStr = "SELECT w FROM WatchEntity w";
        Query query = this.entityManager.createQuery(queryStr);
        return query.getResultList();
    }

    /**
     * If there's already an entry in database with PK @sku the validation fails.
     *
     * @param sku
     * @throws ApplicationException
     */
    private void validateWatchBeforePersist(String sku) throws ApplicationException {
        WatchEntity watchEntity = this.findBySky(sku);
        if (watchEntity != null) {
            throw new ApplicationException(sku, ExceptionReason.DUPLICATE_KEY);
        }
    }

    public List<String> autoCompleteSku(String prefix) {
        final String queryStr = "SELECT w.sku FROM WatchEntity w WHERE w.sku like :prefix";
        Query query = this.entityManager.createQuery(queryStr);
        query.setParameter("prefix", prefix + "%");
        return query.getResultList();
    }
}
