package cloudcomputing.assignment2.util;

import cloudcomputing.assignment2.dto.WatchDto;
import cloudcomputing.assignment2.entity.WatchEntity;
import cloudcomputing.assignment2.exception.ApplicationException;
import cloudcomputing.assignment2.exception.ExceptionReason;
import cloudcomputing.assignment2.type.Gender;
import cloudcomputing.assignment2.type.WatchStatus;
import cloudcomputing.assignment2.type.WatchType;
import org.springframework.stereotype.Component;

import static cloudcomputing.assignment2.util.EnumUtility.enumValuesToString;

import java.util.ArrayList;
import java.util.List;

/**
 * Dto to entity and vice-versa mapper.
 */
@Component
public class Mapper {

    public WatchEntity toEntity(WatchDto watchDto) throws ApplicationException {
        WatchEntity watchEntity = new WatchEntity();
        watchEntity.setBraceletMaterial(watchDto.getBracelet_material());
        watchEntity.setCaseForm(watchDto.getCase_form());
        watchEntity.setDialColor(watchDto.getDial_color());
        watchEntity.setCaseMaterial(watchDto.getCase_material());
        watchEntity.setDialMaterial(watchDto.getDial_material());

        try {
            watchEntity.setGender(Gender.valueOf(watchDto.getGender()));
        } catch (IllegalArgumentException | NullPointerException e) {
            throw new ApplicationException("Field gender must not have values other than : " + enumValuesToString(Gender.man), ExceptionReason.ENUM_EXCEPTION);
        }

        try {
            watchEntity.setStatus(WatchStatus.valueOf(watchDto.getStatus()));
        } catch (IllegalArgumentException | NullPointerException e) {
            throw new ApplicationException("Field status must not have values other than : " + enumValuesToString(WatchStatus.current), ExceptionReason.ENUM_EXCEPTION);
        }

        try {
            watchEntity.setType(WatchType.valueOf(watchDto.getType()));
        } catch (IllegalArgumentException | NullPointerException e) {
            throw new ApplicationException("Field type must not have values other than : " + enumValuesToString(WatchType.chrono), ExceptionReason.ENUM_EXCEPTION);
        }

        watchEntity.setMovement(watchDto.getMovement());
        watchEntity.setSku(watchDto.getSku());
        if (watchDto.getYear() == null) {
            throw new ApplicationException("Field year should be provided.", ExceptionReason.MISSING_FIELD);
        }
        watchEntity.setYear(watchDto.getYear());
        return watchEntity;
    }

    public WatchDto toDto(WatchEntity watchEntity) {
        WatchDto watchDto = new WatchDto();
        watchDto.setBracelet_material(watchEntity.getBraceletMaterial());
        watchDto.setCase_form(watchEntity.getCaseForm());
        watchDto.setCase_material(watchEntity.getCaseMaterial());
        watchDto.setDial_color(watchEntity.getDialColor());
        watchDto.setDial_material(watchEntity.getDialMaterial());
        watchDto.setGender(watchEntity.getGender() != null ? watchEntity.getGender().name() : null);
        watchDto.setMovement(watchEntity.getMovement());
        watchDto.setSku(watchEntity.getSku());
        watchDto.setStatus(watchEntity.getStatus() != null ? watchEntity.getStatus().name() : null);
        watchDto.setType(watchEntity.getType() != null ? watchEntity.getType().name() : null);
        watchDto.setYear(watchEntity.getYear());
        return watchDto;
    }

    public List<WatchEntity> toEntityList(List<WatchDto> watchDtos) throws ApplicationException {
        List<WatchEntity> watchEntities = new ArrayList<>();
        WatchEntity watchEntity;
        for (WatchDto watchDto : watchDtos) {
            watchEntity = this.toEntity(watchDto);
            watchEntities.add(watchEntity);
        }
        return watchEntities;
    }

    public List<WatchDto> toDtoList(List<WatchEntity> watchEntities) {
        List<WatchDto> watchDtos = new ArrayList<>();
        WatchDto watchDto;
        for (WatchEntity watchEntity : watchEntities) {
            watchDto = this.toDto(watchEntity);
            watchDtos.add(watchDto);
        }
        return watchDtos;
    }

}
