package cloudcomputing.assignment2.interceptor;

import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

@Component
public class HttpRequestInterceptor implements HandlerInterceptor {

    /**
     * Handles every http request and attaches cache control header for 1 hour in every GET request.
     *
     * @param request  The request to be intercepted
     * @param response The respose
     * @param handler
     * @return
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (isGetRequest(request)) {

            String cacheHeaderValue = CacheControl.maxAge(1, TimeUnit.HOURS).getHeaderValue();
            response.setHeader(HttpHeaders.CACHE_CONTROL, cacheHeaderValue);

            Instant instant = Instant.now().plus(1, ChronoUnit.HOURS);
            String formattedTime = DateTimeFormatter.RFC_1123_DATE_TIME
                    .withZone(ZoneOffset.UTC)
                    .format(instant);
            response.setHeader(HttpHeaders.EXPIRES, formattedTime);
        }
        return true;
    }

    private boolean isGetRequest(HttpServletRequest request) {
        return "GET".equals(request.getMethod());
    }


}
