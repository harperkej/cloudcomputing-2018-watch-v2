package cloudcomputing.assignment2.entity;

import cloudcomputing.assignment2.type.Gender;
import cloudcomputing.assignment2.type.WatchStatus;
import cloudcomputing.assignment2.type.WatchType;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the watches database table.
 */
@Entity
@Table(name = "watches")
public class WatchEntity implements Serializable {

    @Id
    private String sku;

    @Column(name = "bracelet_material")
    private String braceletMaterial;

    @Column(name = "case_form")
    private String caseForm;

    @Column(name = "case_material")
    private String caseMaterial;

    @Column(name = "dial_color")
    private String dialColor;

    @Column(name = "dial_material")
    private String dialMaterial;

    private String movement;

    @Enumerated(value = EnumType.STRING)
    private WatchStatus status;

    @Enumerated(value = EnumType.STRING)
    private WatchType type;

    @Enumerated(value = EnumType.STRING)
    private Gender gender;

    private int year;

    public WatchEntity() {
    }

    public String getBraceletMaterial() {
        return this.braceletMaterial;
    }

    public void setBraceletMaterial(String braceletMaterial) {
        this.braceletMaterial = braceletMaterial;
    }

    public String getCaseForm() {
        return this.caseForm;
    }

    public void setCaseForm(String caseForm) {
        this.caseForm = caseForm;
    }

    public String getCaseMaterial() {
        return this.caseMaterial;
    }

    public void setCaseMaterial(String caseMaterial) {
        this.caseMaterial = caseMaterial;
    }

    public String getDialColor() {
        return this.dialColor;
    }

    public void setDialColor(String dialColor) {
        this.dialColor = dialColor;
    }

    public String getDialMaterial() {
        return this.dialMaterial;
    }

    public void setDialMaterial(String dialMaterial) {
        this.dialMaterial = dialMaterial;
    }

    public Gender getGender() {
        return this.gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getMovement() {
        return this.movement;
    }

    public void setMovement(String movement) {
        this.movement = movement;
    }

    public String getSku() {
        return this.sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public WatchStatus getStatus() {
        return this.status;
    }

    public void setStatus(WatchStatus status) {
        this.status = status;
    }

    public WatchType getType() {
        return this.type;
    }

    public void setType(WatchType type) {
        this.type = type;
    }

    public int getYear() {
        return this.year;
    }

    public void setYear(int year) {
        this.year = year;
    }

}