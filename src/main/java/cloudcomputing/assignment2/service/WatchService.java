package cloudcomputing.assignment2.service;

import cloudcomputing.assignment2.dto.WatchDto;
import cloudcomputing.assignment2.entity.WatchEntity;
import cloudcomputing.assignment2.exception.ApplicationException;
import cloudcomputing.assignment2.exception.ExceptionReason;
import cloudcomputing.assignment2.repository.WatchRepository;
import cloudcomputing.assignment2.type.Gender;
import cloudcomputing.assignment2.type.WatchStatus;
import cloudcomputing.assignment2.type.WatchType;
import cloudcomputing.assignment2.util.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

import static cloudcomputing.assignment2.util.EnumUtility.enumValuesToString;

@Service
@Transactional
public class WatchService {

    @Autowired
    private WatchRepository watchRepository;

    @Autowired
    private Mapper mapper;

    public WatchEntity create(WatchEntity watchEntity) throws ApplicationException {
        return this.watchRepository.create(watchEntity);
    }

    public WatchEntity findBySku(String sku) throws ApplicationException {
        WatchEntity watchEntity = this.watchRepository.findBySky(sku);
        if (watchEntity == null) {
            throw new ApplicationException("Sku = " + sku, ExceptionReason.RESOURCE_NOT_FOUND);
        }
        return watchEntity;
    }

    public WatchEntity updateBySku(String sku, WatchDto watchDto) throws ApplicationException {
        WatchEntity watchEntity = this.findBySku(sku);
        //update unconditionally not mandatory fields.
        watchEntity.setBraceletMaterial(watchDto.getBracelet_material());
        watchEntity.setCaseForm(watchDto.getCase_form());
        watchEntity.setDialColor(watchDto.getDial_color());
        watchEntity.setCaseMaterial(watchDto.getCase_material());
        watchEntity.setDialMaterial(watchDto.getDial_material());

        if (watchDto.getGender() != null) {
            try {
                watchEntity.setGender(Gender.valueOf(watchDto.getGender()));
            } catch (IllegalArgumentException | NullPointerException e) {
                throw new ApplicationException("Field gender must not have values other than : " + enumValuesToString(Gender.man), ExceptionReason.ENUM_EXCEPTION);
            }
        }

        if (watchDto.getStatus() != null) {
            try {
                watchEntity.setStatus(WatchStatus.valueOf(watchDto.getStatus()));
            } catch (IllegalArgumentException | NullPointerException e) {
                throw new ApplicationException("Field status must not have values other than : " + enumValuesToString(WatchStatus.current), ExceptionReason.ENUM_EXCEPTION);
            }
        }

        if (watchDto.getType() != null) {
            try {
                watchEntity.setType(WatchType.valueOf(watchDto.getType()));
            } catch (IllegalArgumentException | NullPointerException e) {
                throw new ApplicationException("Field type must not have values other than : " + enumValuesToString(WatchType.chrono), ExceptionReason.ENUM_EXCEPTION);
            }
        }

        watchEntity.setMovement(watchDto.getMovement());

        if (watchDto.getYear() != null) {
            watchEntity.setYear(watchDto.getYear());
        }
        return this.watchRepository.update(watchEntity);
    }

    public void delete(String sku) throws ApplicationException {
        WatchEntity watchEntity = this.findBySku(sku);
        this.watchRepository.delete(watchEntity);
    }

    public List<WatchEntity> search(String sku, String type, String status, String gender, Integer year) throws ApplicationException {
        if (type != null) {
            try {
                WatchType.valueOf(type);
            } catch (IllegalArgumentException e) {
                throw new ApplicationException("Field type must not have values other than : " + enumValuesToString(WatchType.chrono), ExceptionReason.ENUM_EXCEPTION);
            }
        }

        if (status != null) {
            try {
                WatchStatus.valueOf(status);
            } catch (IllegalArgumentException e) {
                throw new ApplicationException("Field status must not have values other than : " + enumValuesToString(WatchStatus.current), ExceptionReason.ENUM_EXCEPTION);
            }
        }

        if (gender != null) {
            try {
                Gender.valueOf(gender);
            } catch (IllegalArgumentException e) {
                throw new ApplicationException("Field gender must not have values other than : " + enumValuesToString(Gender.man), ExceptionReason.ENUM_EXCEPTION);
            }
        }

        return this.watchRepository.search(sku, type != null ? WatchType.valueOf(type) : null,
                status != null ? WatchStatus.valueOf(status) : null,
                gender != null ? Gender.valueOf(gender) : null, year);
    }

    public List<WatchEntity> findAll() {
        return this.watchRepository.findAll();
    }

    public List<String> autoCompleteSku(String prefix) {
        return this.watchRepository.autoCompleteSku(prefix);
    }
}
