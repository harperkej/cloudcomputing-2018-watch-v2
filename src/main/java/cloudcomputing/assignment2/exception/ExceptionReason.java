package cloudcomputing.assignment2.exception;


public enum ExceptionReason {

    DUPLICATE_KEY("There is already a watch with the given key."),
    ENUM_EXCEPTION("The provided value is not permitted."),
    RESOURCE_NOT_FOUND("No watch found with the provided sku."),
    MISSING_FIELD("Field is missing.");

    private String reason;

    private ExceptionReason(String casue) {
        this.reason = casue;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
